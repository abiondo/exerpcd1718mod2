package pcd2017.exer2;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.IntConsumer;
import java.util.stream.StreamSupport;

/**
 * Suggested element for the Stream-based algorithm.
 */
class Candidate {
  final int testNonce;
  final String hash;

  Candidate(int testNonce, String hash) {
    this.testNonce = testNonce;
    this.hash = hash;
  }
}

/**
 * A Spliterator over a range of integers, that splits
 * into fixed-size chunks instead of bisecting.
 * See ANSWER.md for why I'm doing such a thing.
 */
class ChunkedIntSpliterator implements Spliterator.OfInt {
  private long from, to;
  private final int chunkSize;

  /**
   * Constructs a new chunk-splitted integer range Spliterator.
   * @param from Lower range bound, inclusive.
   * @param to Upper range bound, inclusive.
   * @param chunkSize Split chunk size.
   */
  public ChunkedIntSpliterator(int from, int to, int chunkSize) {
    this.from = from;
    this.to = to;
    this.chunkSize = chunkSize;
  }

  @Override
  public boolean tryAdvance(IntConsumer action) {
    if (from <= to) {
      action.accept((int)from++);
      return true;
    }
    return false;
  }

  @Override
  public Spliterator.OfInt trySplit() {
    if (estimateSize() <= chunkSize)
      return null;

    // This operation won't overflow.
    // (1) estimateSize() > chunkSize
    //     => to - from + 1 > chunkSize
    //     => from + chunkSize - 1 < to
    // (2) ctor takes from as int
    //     => Integer.MIN_VALUE <= from <= Integer.MAX_VALUE
    // (3) ctor takes to as int
    //     => Integer.MIN_VALUE <= to <= Integer.MAX_VALUE
    // (int)from won't truncate because of (2).
    // By (1) and (3) we have from + chunkSize - 1 < Integer.MAX_VALUE
    int lhsTo = (int)from + chunkSize - 1;
    ChunkedIntSpliterator split = new ChunkedIntSpliterator((int)from, lhsTo, chunkSize);
    // From previous proof we know lhsTo + 1 <= Integer.MAX_VALUE, invariant (2) is kept
    from = lhsTo + 1;

    return split;
  }

  @Override
  public long estimateSize() {
    return to - from + 1;
  }

  @Override
  public int characteristics() {
    return DISTINCT | IMMUTABLE | NONNULL | ORDERED | SIZED | SORTED | SUBSIZED;
  }

  @Override
  public Comparator<? super Integer> getComparator() {
    // Natural order
    return null;
  }
}

public class Miner {
  /**
   * Parallel-stream-based mining algorithm.
   * 
   * @param data
   * @throws NoSuchElementException
   * @return
   */
  public Block streamMine(BlockData data) throws NoSuchElementException {
    ChunkedIntSpliterator it = new ChunkedIntSpliterator(Integer.MIN_VALUE, Integer.MAX_VALUE, 1000);
    Candidate cand = StreamSupport.stream(it, true)
            .map((nonce) -> new Candidate(nonce, sha256(canonical(data, nonce))))
            .filter(Miner::isTarget)
            .findAny()
            .get();
    return new Block(cand.testNonce, cand.hash, data);
  }

  /**
   * Thread-based mining algorithm.
   * 
   * @param data
   * @return
   * @throws InterruptedException
   * @throws ExecutionException
   */
  public Block threadMine(BlockData data) throws InterruptedException, ExecutionException {
    int numThreads = Runtime.getRuntime().availableProcessors();
    ExecutorService executor = Executors.newFixedThreadPool(numThreads);
    ArrayList<Callable<Candidate>> tasks = new ArrayList<>();

    for (int i = 0; i < numThreads; i++) {
      final int offset = i;
      tasks.add(() -> {
        long nonce = Integer.MIN_VALUE + offset;
        while (!Thread.currentThread().isInterrupted() && nonce <= Integer.MAX_VALUE) {
          String hash = sha256(canonical(data, (int)nonce));
          if (isTarget(hash))
            return new Candidate((int)nonce, hash);
          nonce += numThreads;
        }
        throw new RuntimeException("No good nonce");
      });
    }

    Candidate cand = executor.invokeAny(tasks);
    executor.shutdownNow();
    return new Block(cand.testNonce, cand.hash, data);
  }

  /**
   * Create the canonical representation of the blockdata with the nonce, to be fed to the hash function.
   * 
   * DO NOT MODIFY
   * 
   * @param input data to hash
   * @param nonce nonce to hash with the data
   * @return
   */
  public static String canonical(BlockData input, int nonce) {
    return nonce + ":" + input.previousHash + ":" + input.timestamp + ":"
        + input.data;
  }

  /**
   * Calculate the hash of the input
   * 
   * DO NOT MODIFY
   * 
   * @param input data to hash
   * @return hash value
   */
  public static String sha256(String input) {
    try {
      MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
      byte[] result = sha256.digest(input.getBytes());
      return bytesToHex(result);
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException("This should not have happened");
    }
  }

  /**
   * Pretty-prints a byte array.
   * 
   * DO NOT MODIFY
   * 
   * @param hash
   * @return
   */
  private static String bytesToHex(byte[] hash) {
    StringBuilder hexString = new StringBuilder();
    for (int i = 0; i < hash.length; i++) {
      String hex = Integer.toHexString(0xff & hash[i]);
      if (hex.length() == 1)
        hexString.append('0');
      hexString.append(hex);
    }
    return hexString.toString();
  }

  public static final String HASH_PREFIX = "42424";

  /**
   * Checks if an hash is the target value
   * 
   * DO NOT MODIFY
   * 
   * @param hash the hash to check
   * @return true if it satisfies the target condition
   */
  public static boolean isTarget(Candidate candidate) {
    return candidate.hash.startsWith(HASH_PREFIX);
  }

  /**
   * Checks if an hash is the target value
   * 
   * DO NOT MODIFY
   * 
   * @param hash the hash to check
   * @return true if it satisfies the target condition
   */
  public static boolean isTarget(String hash) {
    return hash.startsWith(HASH_PREFIX);
  }

  /**
   * Checks if the block is valid
   * 
   * DO NOT MODIFY
   * 
   * @param block Block to validate
   * @return true if the block hash is correct and in target
   */
  public static boolean validate(Block block) {
    boolean targetHash = isTarget(block.hash);
    boolean correctHash = block.hash
        .equals(sha256(canonical(block.data, block.nonce)));
    return targetHash && correctHash;
  }

}
