package pcd2017.exer2;

/**
 * A block in the blockchain. It can be verified as good if Miner.sha256(Miner.canonical(data, nonce))==hash
 *
 */
public class Block {
  /**
   * nonce of the block
   */
  public final int nonce;
  /**
   * hashvalue of the block
   */
  public final String hash;
  /**
   * Data carried by the block
   */
  public final BlockData data;

  public Block(int nonce, String hash, BlockData data) {
    this.nonce = nonce;
    this.hash = hash;
    this.data = data;
  }

}
